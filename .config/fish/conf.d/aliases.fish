function checkbin
    if command -v $argv 2>&1 >/dev/null
        true
    else
        false
    end
end

# Shorthands for long commands
checkbin pikaur && abbr -ag pik 'pikaur'
checkbin nvim && abbr -ag v 'nvim'
abbr -ag prsg 'pgrep -afi'
checkbin file && abbr -ag gmime 'file --mime-type -bL'
checkbin pacman && abbr -ag pacman-unused 'sudo pacman -Rns $(pacman -Qtdq)'

# Aliases for default arguments to commands
if checkbin exa
    alias ls 'exa --git --icons --color=auto --group-directories-first' # Use exa with icons and color
    alias ll 'ls -al'
    alias la 'ls -a | wc -l'
else
    alias ll 'ls -Al'
    alias la 'ls -A | wc -l'
end
alias less 'less -R'  # allow ansi color in less
alias diff 'diff --color -u'
checkbin bat && alias cat 'bat -pp'
checkbin mpv && alias mpv 'mpv --volume=30'

# File Movement Aliases
alias rm 'rm -i'
alias cp 'cp -i'
alias mv 'mv -i'
checkbin rsync && alias rsyncp 'rsync -ah --progress'

# Misc
alias wdl 'curl -LO --progress-bar'
checkbin zip && alias zipdir 'zip -r "$(basename $PWD)".zip ./'
checkbin 7zr && alias 7zdir '7zr a "$(basename $PWD)".7z ./'
checkbin reflector && alias fetch-mirrors 'sudo reflector -c "United States" -f5 --save /etc/pacman.d/mirrorlist'

# youtube-dl
if checkbin youtube-dl
    alias ydl 'youtube-dl --embed-thumbnail --add-metadata -i'
    alias ydla 'ydl -x -f bestaudio/best'
end

# git
if checkbin git
    if checkbin dotbare
        alias db 'dotbare'
    end
    alias g 'git'
    alias gb 'g branch'
    alias gst 'g status'
    alias gcl 'g clone --recurse-submodules'
    alias gc 'g commit -v'
    alias gcm 'gc -m'
    alias gf 'g fetch'
    alias gpu 'g pull'
    alias gd 'g diff'
    alias gds 'gd --staged'
    alias ga 'g add'
    alias gaa 'ga --all'
    alias grm 'g rm'
    alias gre 'g restore'
    alias gres 'gre --staged'
    alias gp 'g push'
end
