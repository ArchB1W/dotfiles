bind -k nul forward-char # ^Space
# lfcd with workaround from the lfcd.fish example file
bind \co 'lfcd'
bind \ce edit_command_buffer
