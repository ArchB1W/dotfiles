if status is-login
    fish_add_path -g (du -L "$HOME"/.local/bin | cut -f2)
    fish_add_path -g "$HOME/.nix-profile/bin"

    ## Theming
    # GTK Theme: materia-gtk-theme
    # QT Theme: kvantum-theme-materia (Set with qt6ct and kvantum)
    # Icons: papirus-icon-theme
    # Cursor: xcursor-breeze (KDE Breeze Cursor) (AUR)
    set -gx QT_QPA_PLATFORMTHEME qt6ct
    set -gx QT_STYLE_OVERRIDE kvantum
    set -gx SUDO_PROMPT ' [sudo] password for %p: '
    set -gx FZF_DEFAULT_OPTS "
    --color=fg:#c0caf5,bg:#1a1b26,hl:#bb9af7
    --color=fg+:#c0caf5,bg+:#1a1b26,hl+:#7dcfff
    --color=info:#7aa2f7,prompt:#7dcfff,pointer:#7dcfff
    --color=marker:#9ece6a,spinner:#9ece6a,header:#9ece6a
    "
    set -gx BEMENU_OPTS "
    -p '' -i --list=18 --fn 'Monospace 12'
    --center --width-factor 0.5 --cw 2
    --border=3 --bdr #e0af68
    --tb #e0af68 --tf #24283b
    --fb #24283b --ff #c0caf5
    --nb #24283b --nf #c0caf5
    --ab #24283b --af #c0caf5
    --hb #e0af68 --hf #24283b
    "

    ## ~/ Configuration
    # Hidden Directories
    set -gx XDG_DATA_HOME "$HOME"/.local/share
    set -gx XDG_STATE_HOME "$HOME"/.local/state
    set -gx XDG_CONFIG_HOME "$HOME"/.config
    set -gx XDG_CACHE_HOME "$HOME"/.cache
    # Make things follow the XDG Spec
    set -gx PASSWORD_STORE_DIR "$XDG_DATA_HOME"/password-store
    set -gx WINEPREFIX "$XDG_DATA_HOME"/wineprefixes/default
    set -gx NPM_CONFIG_USERCONFIG "$XDG_DATA_HOME"/npm/npmrc
    set -gx GTK2_RC_FILES "$XDG_DATA_HOME"/gtk-2.0/gtkrc-2.0
    set -gx GRADLE_USER_HOME "$XDG_DATA_HOME"/gradle
    set -gx CARGO_HOME "$XDG_DATA_HOME"/cargo
    set -gx GNUPGHOME "$XDG_DATA_HOME"/gnupg
    set -gx RANDFILE "$XDG_DATA_HOME"/rnd
    set -gx GOPATH "$XDG_DATA_HOME"/go
    set -gx WGETRC "$XDG_CONFIG_HOME"/wgetrc
    set -gx LESSHISTFILE "-"
    # Why is C# like this
    set -gx NUGET_PACKAGES "$XDG_CACHE_HOME"/NuGetPackages
    # Mainly used in my scripts
    set -gx PMCINSTDIR "/media/2TB-Storage/Minecraft/Prism Launcher/instances"

    ## Fixes for applications
    set -gx _JAVA_AWT_WM_NONREPARENTING 1
    set -gx _JAVA_OPTIONS "-Dawt.useSystemAAFontSettings=on -Dswing.aatext=true -Dswing.defaultlaf=com.sun.java.swing.plaf.gtk.GTKLookAndFeel -Djava.util.prefs.userRoot=$XDG_DATA_HOME/java"
    set -gx JAVA_FONTS "/usr/share/fonts/TTF"
    # Get rid of the annoying "Firefox is already running" message
    set -gx MOZ_DBUS_REMOTE "1"
    # Prevents wine from creating .desktop files
    set -gx WINEDLLOVERRIDES "winemenubuilder.exe=d#"
    # Fix big qt Cursor
    set -gx XCURSOR_SIZE 24

    ## Default Programs
    set -gx EDITOR "nvim"
    set -gx VISUAL "nvim"
    set -gx TERMINAL "alacritty"
    set -gx BROWSER "librewolf"
    set -gx READER "zathura"
    set -gx GMENU "bemenu"
    set -gx PAGER "less -R"
    set -gx MANPAGER "nvim +Man\!"

    [ (tty) = "/dev/tty1" ] && exec Hyprland
end

if status is-interactive
    set fish_greeting "$(justfetch)"
    set -e VISUAL # Unset VISUAL so EDITOR is used instead while interactive
    set -gx DOTBARE_DIR "$HOME"/projects/Repositories/Mine/dotfiles

    if [ $TERM != "linux" ]
        lf_icons
    else
        # Tokyonight Storm
        printf "
        \e]P01d202f
        \e]P1f7768e
        \e]P29ece6a
        \e]P3e0af68
        \e]P47aa2f7
        \e]P5bb9af7
        \e]P67dcfff
        \e]P7a9b1d6
        \e]P8414868
        \e]P9f7768e
        \e]PA9ece6a
        \e]PBe0af68
        \e]PC7aa2f7
        \e]PDbb9af7
        \e]PE7dcfff
        \e]PFc0caf5
        "
        # get rid of artifacts
        clear
    end
end
