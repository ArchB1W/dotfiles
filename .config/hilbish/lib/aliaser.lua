local aliaser = {}

local util = require("lib.util")
local hb = hilbish

local hasbin = util.hasbin
local alias = hb.alias

-- aliasTable is parsed in an uncontrollable order
-- So to have some aliases to override some others based on whether a command exists
-- you could have multiple tables and call the loadAliases function multiple times

-- Key is required command or `none`
-- Value is a table of tables with each one containing the short and long of the alias
function aliaser.loadAliases(t)
    for reqCmd, aliases in pairs(t) do
        if reqCmd == "none" or hasbin(reqCmd) then
            for _, aliasPair in pairs(aliases) do
                alias(aliasPair[1], aliasPair[2])
            end
        end
    end
end

return aliaser
