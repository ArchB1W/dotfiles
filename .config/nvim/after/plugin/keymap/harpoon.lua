local map = vim.keymap

-- Harpoon (How did I live without this?)
local harpoon = require("harpoon")
local harpoon_mark = require("harpoon.mark")
local harpoon_cmd_ui = require("harpoon.cmd-ui")
local harpoon_ui = require("harpoon.ui")
local harpoon_term = require("harpoon.term")
harpoon.setup {
    global_settings = {
        -- Automatically run command when it's sent to a terminal
        enter_on_sendcmd = true,
    },
}
-- Mark Management
-- stylua: ignore start
map.set("n", "<leader>ha", harpoon_mark.add_file)
map.set("n", "<leader>hm", harpoon_ui.toggle_quick_menu)
map.set("n", "<leader>hc", harpoon_cmd_ui.toggle_quick_menu)
-- Files
map.set("n", "<A-q>", function() harpoon_ui.nav_file(1) end)
map.set("n", "<A-w>", function() harpoon_ui.nav_file(2) end)
map.set("n", "<A-e>", function() harpoon_ui.nav_file(3) end)
map.set("n", "<A-r>", function() harpoon_ui.nav_file(4) end)
-- Terminals
map.set("n", "<A-z>", function() harpoon_term.gotoTerminal(1) end)
map.set("n", "<A-x>", function() harpoon_term.gotoTerminal(2) end)
map.set("n", "<A-c>", function() harpoon_term.gotoTerminal(3) end)
map.set("n", "<A-S-z>", function() harpoon_term.sendCommand(1, 1) end)
map.set("n", "<A-S-x>", function() harpoon_term.sendCommand(2, 2) end)
map.set("n", "<A-S-c>", function() harpoon_term.sendCommand(3, 3) end)
-- stylua: ignore end
