local action_layout = require("telescope.actions.layout")
require("telescope").setup {
    defaults = {
        mappings = {
            i = {
                ["<M-p>"] = action_layout.toggle_preview,
                ["<M-m>"] = action_layout.toggle_mirror,
            },
        },
    },
}
