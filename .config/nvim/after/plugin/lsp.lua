---@diagnostic disable: need-check-nil

-- Automatically required tools for formatting and LSP
require("mason").setup()
require("mason-tool-installer").setup {
    ensure_installed = {
        -- Lua
        "lua-language-server",
        "stylua",
        -- Python
        "pyright",
        "flake8",
        "autopep8",
        -- C#
        "omnisharp",
        -- Java
        "jdtls",
        -- Rust
        "rust-analyzer",
        -- Nix
        "rnix-lsp",
    },
    auto_update = true,
}

-- Diagnostics Symbols
vim.fn.sign_define(
    "DiagnosticSignError",
    { texthl = "DiagnosticSignError", text = "", numhl = "DiagnosticSignError" }
)
vim.fn.sign_define(
    "DiagnosticSignWarn",
    { texthl = "DiagnosticSignWarn", text = "", numhl = "DiagnosticSignWarn" }
)
vim.fn.sign_define(
    "DiagnosticSignHint",
    { texthl = "DiagnosticSignHint", text = "", numhl = "DiagnosticSignHint" }
)
vim.fn.sign_define(
    "DiagnosticSignInfo",
    { texthl = "DiagnosticSignInfo", text = "", numhl = "DiagnosticSignInfo" }
)

-- nvim-cmp
local cmp = require("cmp")
cmp.setup {
    formatting = {
        format = function(entry, vim_item)
            return require("lspkind").cmp_format {
                mode = "symbol_text",
                menu = {
                    nvim_lsp = "[LSP]",
                    nvim_lsp_signature_help = "[Arg]",
                    vsnip = "[Snippet]",
                    buffer = "[Buffer]",
                    nvim_lua = "[Lua]",
                },
            }(entry, vim_item)
        end,
    },
    -- vsnip as Snippet Engine
    snippet = {
        expand = function(args)
            vim.fn["vsnip#anonymous"](args.body)
        end,
    },
    mapping = cmp.mapping.preset.insert {
        ["<C-b>"] = cmp.mapping.scroll_docs(-4),
        ["<C-f>"] = cmp.mapping.scroll_docs(4),
        ["<C-Space>"] = cmp.mapping.complete(),
        ["<C-e>"] = cmp.mapping.abort(),
        -- Accept currently selected item.
        -- Set `select` to `false` to only confirm explicitly selected items.
        ["<CR>"] = cmp.mapping.confirm { select = true },
    },
    sources = cmp.config.sources {
        { name = "nvim_lsp" },
        { name = "nvim_lsp_signature_help" },
        { name = "vsnip" },
        { name = "buffer" },
    },
}

-- Use buffer source for `/` and `?`
cmp.setup.cmdline({ "/", "?" }, {
    mapping = cmp.mapping.preset.cmdline(),
    sources = {
        { name = "buffer" },
    },
})

-- Use cmdline for `:`
cmp.setup.cmdline(":", {
    mapping = cmp.mapping.preset.cmdline(),
    sources = cmp.config.sources {
        { name = "path" },
        { name = "cmdline" },
    },
})

-- Use nvim-lua completion for lua filetype
cmp.setup.filetype("lua", {
    sources = cmp.config.sources {
        { name = "nvim_lua" },
        { name = "nvim_lsp" },
        { name = "nvim_lsp_signature_help" },
        { name = "vsnip" },
        { name = "buffer" },
    },
})

-- Language Servers
local capabilities = require("cmp_nvim_lsp").default_capabilities()
local on_attach = function() end

-- Python
require("lspconfig").pyright.setup {
    on_attach = on_attach,
    capabilities = capabilities,
}

-- Rust
require("lspconfig").rust_analyzer.setup {
    on_attach = on_attach,
    capabilities = capabilities,
}

-- Nix
require("lspconfig").rnix.setup {
    on_attach = on_attach,
    capabilities = capabilities,
}

-- Lua
require("lspconfig").sumneko_lua.setup {
    on_attach = on_attach,
    capabilities = capabilities,
    settings = {
        Lua = {
            runtime = {
                -- Tell the language server which version of Lua you're using
                -- (most likely LuaJIT in the case of Neovim)
                version = "LuaJIT",
            },
            diagnostics = {
                -- Get the language server to recognize the `vim` global
                globals = { "vim" },
                disable = { "redefined-local" },
            },
            workspace = {
                checkThirdParty = false,
                -- Make the server aware of Neovim runtime files
                library = {
                    vim.api.nvim_get_runtime_file("", true),
                },
                preloadFileSize = 1000,
            },
            -- Do not send telemetry data containing a randomized but unique identifier
            telemetry = {
                enable = false,
            },
        },
    },
}

-- C#
require("lspconfig").omnisharp.setup {
    on_attach = on_attach,
    capabilities = capabilities,
    cmd = { "omnisharp" },

    -- Enables support for reading code style, naming convention and analyzer
    -- settings from .editorconfig.
    enable_editorconfig_support = true,

    -- If true, MSBuild project system will only load projects for files that
    -- were opened in the editor. This setting is useful for big C# codebases
    -- and allows for faster initialization of code navigation features only
    -- for projects that are relevant to code that is being edited. With this
    -- setting enabled OmniSharp may load fewer projects and may thus display
    -- incomplete reference lists for symbols.
    enable_ms_build_load_projects_on_demand = false,

    -- Enables support for roslyn analyzers, code fixes and rulesets.
    enable_roslyn_analyzers = true,

    -- Specifies whether 'using' directives should be grouped and sorted during
    -- document formatting.
    organize_imports_on_format = false,

    -- Enables support for showing unimported types and unimported extension
    -- methods in completion lists. When committed, the appropriate using
    -- directive will be added at the top of the current file. This option can
    -- have a negative impact on initial completion responsiveness,
    -- particularly for the first few completion sessions after opening a solution.
    enable_import_completion = false,

    -- Specifies whether to include preview versions of the .NET SDK when
    -- determining which version to use for project loading.
    sdk_include_prereleases = true,

    -- Only run analyzers against open files when 'enableRoslynAnalyzers' is true
    analyze_open_documents_only = false,
}
