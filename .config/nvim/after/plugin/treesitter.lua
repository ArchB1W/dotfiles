require("nvim-treesitter.configs").setup {
    ensure_installed = "all",

    -- Install parsers synchronously (only applied to `ensure_installed`)
    sync_install = false,

    highlight = {
        enable = true,
        additional_vim_regex_highlighting = false,
        disable = { "diff" },
    },

    rainbow = {
        enable = true,
        extended_mode = true,
        max_file_lines = nil,
    },

    incremental_selection = {
        enable = true,
        keymaps = {
            -- maps in normal mode to init the node/scope selection
            init_selection = "<M-s>",
            -- increment to the upper named parent
            node_incremental = "<M-s>",
            -- decrement to the previous node
            node_decremental = "<M-S-s>",
            -- increment to the upper scope (as defined in locals.scm)
            scope_incremental = "<M-d>",
        },
    },

    textobjects = {
        move = {
            enable = true,
            set_jumps = true,

            goto_next_start = {
                ["]p"] = "@parameter.inner",
                ["]]"] = "@class.outer",
                ["]f"] = "@function.outer",
            },
            goto_next_end = {
                ["]F"] = "@function.outer",
            },
            goto_previous_start = {
                ["[p"] = "@parameter.inner",
                ["[["] = "@class.outer",
                ["[f"] = "@function.outer",
            },
            goto_previous_end = {
                ["[F"] = "@function.outer",
            },
        },

        select = {
            enable = true,
            keymaps = {
                ["af"] = "@function.outer",
                ["if"] = "@function.inner",

                ["ac"] = "@conditional.outer",
                ["ic"] = "@conditional.inner",
                ["aC"] = "@class.outer",
                ["iC"] = "@class.inner",

                ["aa"] = "@parameter.outer",
                ["ia"] = "@parameter.inner",

                ["av"] = "@variable.outer",
                ["iv"] = "@variable.inner",
            },
        },
    },
}
