local capabilities = require("cmp_nvim_lsp").default_capabilities()
local on_attach = function() end

local rootDir = require("jdtls.setup").find_root {
    ".git",
    "mvnw",
    "gradlew",
    "pom.xml",
    "build.gradle",
}
if rootDir == "" then
    return
end

local projectName = vim.fn.fnamemodify(vim.fn.getcwd(), ":p:h:t")
local workspaceDir = vim.fn.stdpath("cache") .. "/../jdtls/workspace/" .. projectName

require("jdtls.setup").add_commands()

require("jdtls").start_or_attach {
    on_attach = on_attach,
    capabilities = capabilities,
    cmd = {
        "jdtls",
        "-data",
        workspaceDir,
    },
    root_dir = rootDir,
    settings = {
        java = {
            completion = { enabled = true },
            configuration = {
                -- See https://github.com/eclipse/eclipse.jdt.ls/wiki/Running-the-JAVA-LS-server-from-the-command-line#initialize-request
                -- And search for `interface RuntimeOption`
                -- The `name` is NOT arbitrary, it must match one of the elements from
                -- `enum ExecutionEnvironment` in the link above
                runtimes = {
                    {
                        name = "JavaSE-1.8",
                        path = "/usr/lib/jvm/java-8-openjdk/",
                    },
                    {
                        name = "JavaSE-11",
                        path = "/usr/lib/jvm/java-11-openjdk/",
                    },
                    {
                        name = "JavaSE-17",
                        path = "/usr/lib/jvm/java-17-openjdk/",
                    },
                    {
                        name = "JavaSE-19",
                        path = "/usr/lib/jvm/java-19-openjdk/",
                    },
                },
            },
        },
    },
}
